## Tuesday, April 7th Meeting

### 1- Introduction (20 min)

#### a. Introduction to the learning circle (10 min, main room)

Links to resources are at the top of this page.

**Main room:** https://meet.jit.si/covid19-learning-circle  
**Breakout room 1:** https://meet.jit.si/covid19-learning-circle-room1  
**Breakout room 2:** https://meet.jit.si/covid19-learning-circle-room2  
**Breakout room 3:** https://meet.jit.si/covid19-learning-circle-room3  

#### b. Feedback review (5 min, main room)

#### c. Questions (5 min, main room)


### 2- Risk communication and community engagement (20 min)

This activity is inspired from the **Think-Pair-Share** activity described here: https://ocw.mit.edu/courses/chemistry/5-95j-teaching-college-level-science-and-engineering-fall-2015/instructor-insights/active-learning-strategy-think-pair-share/

#### a. Think: Find examples of risk communication and community engagement (6 min, alone/etherpad)

Write below your name and a few examples of risk communication and community engagement related to COVID-19 that you have experienced.

The topic of risk communication and community engagement is discusse in step 1.11: https://www.futurelearn.com/courses/covid19-novel-coronavirus/1/steps/710507

From the course: *"Risk communication and community engagement (RCCE) is a critical component of the response to any infectious disease outbreak. What this looks like depends very much on the context and the disease in question [...] Community engagement is the process of supporting communities to consider themselves partners in an outbreak response, and to have ownership in controlling an outbreak."*

...  
...  
...  
...  

#### b. Pair (7 min, breakout rooms)

Join one of the breakout rooms and **write your name** in front of the room you are joining. Once you join a breakout room, **mute yourself from the main room**. In each breakout room, **introduce each other** (e.g., tell about your background, interest in joining this learning circle, or how your life was impacted by the COVID-19 outbreak). Then **discuss risk communication and community engagement** examples from step a. For every subsequent group discussion, you will keep meeting in the same room.

Breakout room 1: ...  
Breakout room 2: ...  

#### c. Share (7 min, main room)

Each group designates a reporter who sums up their discussion with the larger group. Anyone may ask questions or respond to them.

### 3- Quarantine and airport screening (30 min)

#### a. Watch the video: "Quarantine - why do it?" (5 min, alone)

Step 2.5: https://www.futurelearn.com/courses/covid19-novel-coronavirus/1/steps/710514

#### b. Read the text: "Evidence base for airport screening" (5 min, alone)

Step 2.6: https://www.futurelearn.com/courses/covid19-novel-coronavirus/1/steps/710513

#### c. Join your breakout room and discuss the video and text, trying to answer the questions outlined under the video (5 min, breakout rooms)

If you have extra time left, you can also discuss the links in the "See Also" section of both steps.

Questions:  
- Describe concepts of incubation period and infectious periods and how these relate to symptoms of COVID-19  
- Explain problems with establishing the incubation period  
- Appraise the implications of these measures for quarantine  

#### d. Simulation (5 min, breakout rooms)

Using the webapp introduced in the course (step 2.6), **determine under which conditions airport screening might be  effective**, then **discuss** whether these conditions are met in the case of COVID-19.  
For this exercise, **use a copy of the webapp** as the public instance is too slow: https://svigneau.shinyapps.io/airport_screening/  
**Write down** below some of the changes you made and the resulting outcome as percentage of cases not detected.  

variable: initial value, percentage not detected -> final value, percentage not detected  
...  
...  
...  
...  
...  

The webapp was published in this article: https://cmmid.github.io/visualisations/traveller-screening  
Its source code can be found here: https://cmmid.github.io/visualisations/traveller-screening  

#### e. Discuss findings (10 min, main room)

Each group designates a reporter who sums up their findings with the larger group. Anyone may ask questions or respond to them.


### 4- Response to COVID-19 in different parts of the world (30 min)

This activity is inspired from the **Jigsaw** activity described here: https://ocw.mit.edu/courses/chemistry/5-95j-teaching-college-level-science-and-engineering-fall-2015/instructor-insights/active-learning-strategy-jigsaw/

#### a. Each group watches a different video (10 min, alone)

If you have extra time left, explore links in the "See Also" section below the video.

**Room 1: Watch video "Response in China"**  
Step 2.9: https://www.futurelearn.com/courses/covid19-novel-coronavirus/1/steps/733900  

**Room 2: Watch video "Response in Africa"**  
Step 2.11: https://www.futurelearn.com/courses/covid19-novel-coronavirus/1/steps/710517  

**Room 3: Watch video "Response in Singapore"**  
Step 2.10: https://www.futurelearn.com/courses/covid19-novel-coronavirus/1/steps/733903  

#### b. Homogeneous group discussion (10 min, breakout rooms)

Participants in each group discuss the video they have watched, trying to answer the questions below when applicable.
If you have extra time left, you can also discuss links in the "See Also" section below the video.

Questions:  
- Describe national or regional activities in response to the COVID-19 outbreak  
- Examine measures planned, required, or taken  
- Assess the impact of these measures, including the social and economic effects of the actions taken  

#### c. Heterogeneous group discussion (10 min, main room)

Each group designates a reporter who sums up their findings with the larger group. Anyone may ask questions or respond to them.

### 5- Leave feedback

Feedback form: [link to form]

### 6- Homework

- Complete remaining steps from Week 2  
- Watch and read Week 3 content  

### 7- Links worth sharing (write below)

Modeling COVID-19 Spread vs Healthcare Capacity: https://alhill.shinyapps.io/COVID19seir/

...