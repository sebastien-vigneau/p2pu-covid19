# Learning Circle: COVID-19, Tackling the Novel Coronavirus

The outline of each meeting will be provided on this pad.  
Anyone with access to the pad is encouraged to edit it, for instance to ask questions or to discuss topics in the learning circle.


## Resources

**"COVID-19: Tackling the Novel Coronavirus" online course:** https://www.futurelearn.com/courses/covid19-novel-coronavirus

**This Etherpad:** https://pad.hadoly.fr/p/9fnd-covid19-learning-circle?lang=en.  
**"..."** are prompts for anyone to edit this Etherpad as part of an activity.  
Use the Etherpad chat for questions or chat.

**Jitsi rooms:**
- **Main room:** https://meet.jit.si/covid19-learning-circle
- **Breakout room 1:** https://meet.jit.si/covid19-learning-circle-room1
- **Breakout room 2:** https://meet.jit.si/covid19-learning-circle-room2
- **Breakout room 3:** https://meet.jit.si/covid19-learning-circle-room3
- **Breakout room 4:** https://meet.jit.si/covid19-learning-circle-room4
- **Breakout room 5:** https://meet.jit.si/covid19-learning-circle-room5
- **Breakout room 6:** https://meet.jit.si/covid19-learning-circle-room6


**When joining a breakout room, mute yourself from the main room, and vice versa.**  
Jitsi works better on Chrome/Chromium.  
Share screen with "Share your screen" button (bottom left).  
Signal you want to ask a question using "Raise/Lower your hand" button (bottom left).  
Chat using the "Open/Close chat" button (bottom left).  
Rooms are always open, discussions can continue after the official end of the meeting.

**BosLab events on Meetup:** https://www.meetup.com/BosLab/events/

## Tuesday, March 31st Meeting

### 1- Introduction (30 min)

#### a. Introduction to the learning circle (10 min, main room)

#### b. Questions (5 min, main room)

#### c. Introduce each other on the Etherpad (5 min, main room)

Write below your name and one or two sentences about yourself:

...  
...  
...  
...  

#### d. Introduce each other on Jitsi (10 min, breakout rooms)

Join one of the breakout rooms and **write your name** in front of the room you are joining. Once you join a breakout room, **mute yourself from the main room**. In each breakout room, **introduce each other** (e.g., tell about your background, interest in joining this learning circle, or how your life was impacted by the COVID-19 outbreak). For every subsequent group discussion, you will keep meeting in the same room.

Breakout room 1: ...  
Breakout room 2: ...  

### 2- Overview of the coronavirus and COVID-19

#### a. Watch the video "Overview of the coronavirus and COVID-19" (5 min, alone)

Step 1.5: https://www.futurelearn.com/courses/covid19-novel-coronavirus/1/steps/710502

#### b. Read the text: "What are the key points in the outbreak to date?" (5 min, alone)

Step 1.6: https://www.futurelearn.com/courses/covid19-novel-coronavirus/1/steps/710503

#### c. Join your breakout room and discuss the video and text, trying to answer the questions outlined under the video (10 min, breakout rooms)

If you have extra time left, open and discuss links in the "See Also" section of both steps.

Questions:
- Describe what is known about the new coronavirus (SARs-CoV-2)
- Where the virus came from and how it spreads
- How COVID-19 compares to diseases caused by other coronaviruses
- How we measure severity of disease

#### d. Beach Ball: Ask each other questions (10 min, main room)

This activity is inspired from the Beach Ball activity described here: https://ocw.mit.edu/courses/chemistry/5-95j-teaching-college-level-science-and-engineering-fall-2015/instructor-insights/active-learning-strategy-beach-ball/

Rules:
- **All participants are in the main room.**
- One participant asks a question to another partipant.
- The designated participant either answers the question or says "Pass" and asks the same question to another participant.
- After a participant answers a question, they can either ask the same or a different question to another participant.
- Questions should be answerable with content in step 1.5 or 1.6.
- Try and ask questions of general interest.


### 3- How was the novel coronavirus identified? (30 min)

#### a. Watch the video "How was the novel coronavirus identified?" (5 min, alone)

Step 1.7: https://www.futurelearn.com/courses/covid19-novel-coronavirus/1/steps/712560

#### b. Read the text: "How can we improve detection?" (5 min, alone)

Step 1.8: https://www.futurelearn.com/courses/covid19-novel-coronavirus/1/steps/712616

#### c. Join your breakout room and discuss the video and text, trying to answer the questions outlined under the video (7 min, breakout rooms)

If you have extra time left, open and discuss links in the "See Also" section of both steps.

Questions:
- Describe the process that led to identification of the novel coronavirus
- Explain the process of identification using whole genome sequencing
- Explain how the initial PCR diagnostic was developed

#### d. Snowball: Define different types of tests (7 min, breakout rooms)

Inspired by activity during P2PU annual gathering (name is mine - I can't remember the original name).

Rules:
- Participants in each breakout room work as a team.
- Each team designates a person in charge of writing on the Etherpad.
- Each team finds a definition for the terms below and writes it on the Etherpad.
- If a definition has already been written by another team, each team can modify or expand it.

Terms to define:

*Molecular test:* ...  

*Simple-to-use test:* ...  

*Serological test:* ...  


#### e. Read and comment on definition (7 min, main room)

Participants read the definitions and comment or ask for clarification.

### 4- How infectious is the virus? (24 min)

#### a. Watch video "How infectious is the virus?" (5 min, alone)

Step 1.9: https://www.futurelearn.com/courses/covid19-novel-coronavirus/1/steps/710505

#### b. Read the article: "Why outbreaks like coronavirus spread exponentially, and how to 'flatten the curve'" from the Washington Post (5 min, alone)

https://www.washingtonpost.com/graphics/2020/world/corona-simulator/

#### c. Join your breakout room and discuss the video and text, trying to answer the questions outlined under the video (7 min, breakout rooms)

If you have extra time left, open and discuss links in the "See Also" section of both steps.

Questions:
- Describe the concept of R0 and secondary attack rate
- Describe how to measure R0 and secondary attack rates
- Describe and explain what we know about the transmissibility of SARs-CoV-2

#### d. Jigsaw, part 1: Within each breakout room, pick one of the four simulations from the Washington Post article and discuss how it relates to the concepts presented in the course (7 min, breakout rooms)

This activity is inspired from the Jigsaw activity described here: https://ocw.mit.edu/courses/chemistry/5-95j-teaching-college-level-science-and-engineering-fall-2015/instructor-insights/active-learning-strategy-jigsaw/

Rules:
- Participants in each breakout room work as a team.
- Each team chooses a simulation and write their room number in front of it.
- Each team discusses how concepts from the course are used (or not) in the simulation.
- Each team designates a person in charge of summarizing their discussion to other participants.
- Everyone reconvene in the main room.

Simulations:

*Free-for-all:* ...  
*Attempted quarantine:* ...  
*Moderate social distancing:* ...  
*Extensive social distancing:* ...  

#### e. Jigsaw, part 2: In main room, each team presents their findings to other participants (7 min, main room)

### 5- Leave feedback

Feedback form: [link to form]

### 6- Homework

- Complete steps 1.10-1.13 from Week 1
- Think about how risk communications and engagement (see step 1.11 for definition) has been performed in your community. This will be used as a discussion prompt during next meeting.
- Watch and read Week 2 content

### 7- Links worth sharing (write below)

...